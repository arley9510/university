<?php

class Pila
{

    /**
     * @var $stack
     * El objeto que contiene la Pila
     */
    private $stack;

    /**
     * Constructor. Crea la pila
     */
    public function __construct()
    {
        $this->stack = array();
    }

    /**
     * Inserta un elemento en el tope de la pila.
     *
     * @param mixed $v Elemento a insertar
     */
    public function push($v)
    {
        $this->stack[] = $v;
    }

    /**
     * Imprime la pila
     */
    public function show()
    {
        print_r($this->stack);
    }

    /**
     * Remueve el elemento al tope de la pila
     *
     * @return mixed El elemento del tope.
     * Si la pila está vacía devolverá NULL
     */
    public function pop()
    {
        return array_pop($this->stack);
    }

    /**
     * Checa si la pila está vacía
     *
     * @return boolean True si está vacía, false caso contrario
     */
    public function isEmpty()
    {
        return empty($this->stack);
    }

    /**
     * Cuenta el tamaño de la pila
     *
     * @return int El tamaño de la pila
     */
    public function length()
    {
        return count($this->stack);
    }

    /**
     * Observa el ultimo elemento de la pila, sin removerlo
     *
     * @return mixed El último elemento de la pila
     */
    public function peek()
    {
        return $this->stack[($this->length() - 1)];
    }
}

class cola {
    /**
     * @var $cola
     * El objeto que contiene la cola
     */
    private $cola;

    /**
     * Constructor. Crea la pila
     */
    public function __construct()
    {
        $this->cola = array();
    }

    /**
     * Inserta un elemento en la cola.
     *
     * @param mixed $v Elemento a insertar
     */
    public function push($v)
    {
        array_push($this->cola, $v);
    }

    /**
     * Imprime la cola
     */
    public function show()
    {
        print_r($this->cola);
    }

    /**
     * Remueve el primer elemto de la cola
     * @return mixed
     */
    public function pop()
    {
        return array_shift($this->cola);
    }

    /**
     * Inserta al inicio de la cola un elemento
     * @param $v
     * @return int
     */
    public function pushFirst($v)
    {
        return array_unshift($this->cola, $v);
    }


}

class lista {

    public $value;

}
