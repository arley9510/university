<?php

require('taller.php');

    /**
     * Se crea una nueva pila
     */
        $stack = new Pila();

    /**
     * Se insertan elementos a la pila
     */
        $stack->push('Hola');
        $stack->push('Mundo');
        $stack->push('Amigos');
        $stack->push('de');
        $stack->push('la');
        $stack->push('u');

        echo "<h1>Pila</h1>";
    /**
     * Se imprime la longitud de la pila
     */
        echo "<h1>" . $stack->length() . "</h1>";

    /**
     * Se elimina el ultimo elemento de la pila
     */
        $stack->pop();

    /**
     * se muestra el ultimo elemento de la pila
     */
        if (!$stack->isEmpty()) {
            echo "<h2>" . $stack->peek() . "</h2>";
        }

        echo "</br>";

        $stack->show();

        echo "<h1>Cola</h1>";

    /**
     * se crea una nueva cola
     */
        $cola = new cola();

    /**
     * insertamos elementos en la cola
     */
        $cola->push(1);
        $cola->push(2);
        $cola->push(3);
        $cola->push(4);
        $cola->push(5);

        echo "</br>";

    /**
     * mostramos la cola
     */
        $cola->show();

    /**
     * eliminamos el primer elemento de la cola
     */
        $cola->pop();

        echo "</br>";

    /**
     * mostramos la cola
     */
        $cola->show();

        $cola->pushFirst(6);

        echo "</br>";

    /**
     * mostramos la cola
     */
    $cola->show();

    echo "</br>";

    echo "<h1>Lista</h1>";

    /**
     * generamos 3 objetos
     */
        $object1 = new lista();
        $object2 = new lista();
        $object3 = new lista();

    /**
     * Asignamos valores a los objetos
     */
        $object1->value = 111;
        $object2->value = 222;
        $object3->value = 333;

    /**
     * creamos una lista
     */
        $list = array();

    /**
     * añadimos los objetos a la lista
     */
        $list[] = $object1;
        $list[] = $object2;
        $list[] = $object3;

    /**
     * imprimimos la lista
     */
        for($i = 0; $i < count($list); $i++) {
            echo $list[$i]->value . ", " ;
        }

    /**
     * Eliminamos el ultimo elemento de la lista
     */
        array_pop($list);

        echo "</br>";

    /**
     * imprimimos la lista
     */
        for($i = 0; $i < count($list); $i++) {
            echo $list[$i]->value . ", " ;
        }

        echo "</br>";

    /**
     * elimina el primer elemento de la lista
     */
        array_shift($list);

    /**
     * imprimimos la lista
     */
        for($i = 0; $i < count($list); $i++) {
            echo $list[$i]->value . ", " ;
        }