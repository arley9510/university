<?php

class campeonato
{
    private $team;
    private $player;
    private $group;
    private $teamLimit;
    private $teamCount;
    private $groupCount;

    /**
     * campeonato constructor.
     */
    public function __construct()
    {
        $this->team = array();
        $this->player = array();
        $this->group = array();
        $this->teamLimit = 0;
        $this->teamCount = 0;
        $this->groupCount = 0;
    }

    /**
     * Operaciones para los equipos
     */

    /**
     * @param int $teamLimit
     */
    public function setTeamLimit($teamLimit)
    {
        if ($teamLimit > 0 || is_numeric($teamLimit)) {
            $this->teamLimit = $teamLimit;
        } else {
            $this->teamLimit = 0;

            echo "<h1>Ingrese un valor valido</h1>";
        }
    }

    /**
     * @return int
     */
    public function getTeamLimit()
    {
        return $this->teamLimit;
    }

    /**
     * @param $team
     */
    public function addTeam($team)
    {
        if ($this->teamLimit > 0) {
            if ($this->teamCount <= $this->teamLimit - 1) {
                $this->team[$this->teamCount] = ['nombre' => $team];
                $this->teamCount++;
            } else {

                echo "<h1>limite alcansado</h1>";
            }
        }
    }

    /**
     * @param $team
     * @return false|int|string
     */
    public function getTeam($team)
    {
        return array_search($team, array_column($this->team, 'nombre'));
    }

    /**
     * @param $team
     * @return mixed
     */
    public function getTeamForId($team)
    {
        return $this->team[$team];
    }


    /**
     * @param $team
     */
    public function dropTeam($team)
    {
        unset($this->team[$team]);
    }

    /**
     * @return array
     */
    public function getTeams()
    {
        return $this->team;
    }

    /**
     * operaciones para los jugadores
     */

    /**
     * @param $team
     * @param $player
     * @return array|string
     */
    public function addPlayer($team, $player)
    {
        if (!is_nan($team) || !is_array($player)) {

            return $this->player[] = ['equipo' => $team, 'jugador' => $player];
        } else {

            return "</h1>Información erronea</h1>";
        }
    }

    /**
     * @param $player
     * @return false|int|string
     */
    public function searchPlayer($player)
    {
        return array_search($player, $this->player);
    }

    /**
     * @return array
     */
    public function getPlayers()
    {
        return $this->player;
    }

    /**
     * @param $player
     */
    public function dropPlayer($player)
    {
        unset($this->player[$this->searchPlayer($player)]);
    }

    /**
     * Operaciones para los grupos
     */

    /**
     *
     */
    public function addGroups()
    {
        if ($this->teamCount > 0) {
            $this->group = array_chunk($this->team, $this->teamCount);
            $this->groupCount++;
        } else {
            echo "<h1>No hay equipos que agregar</h1>";
        }

    }

    /**
     * @return array|string
     */
    public function getGroups()
    {
        if ($this->groupCount > 0) {

            return $this->group;
        } else {

            return "</h1>No hay grupos</h1>";
        }
    }
}

