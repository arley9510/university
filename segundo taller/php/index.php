<?php

require('campeonato.php');
require('navigation.php');

$navbar = new navigation();

$equipo = new campeonato();

/**
 * limite de equipos
 */
$equipo->setTeamLimit(10);

/**
 * equipos en el toeneo
 */
$equipo->addTeam('Atlético Nacional');
$equipo->addTeam('América de Cali');
$equipo->addTeam('Atlético Huila');
$equipo->addTeam('Deportivo Boyacá Chicó');
$equipo->addTeam('Deportivo Cali');
$equipo->addTeam('Millonarios');
$equipo->addTeam('Deportivo Pasto');
$equipo->addTeam('Independiente Santa Fe');
$equipo->addTeam('La Equidad');
$equipo->addTeam('Cúcuta Deportivo');

$equipo->addGroups();

$equipo->addPlayer($equipo->getTeam('Atlético Nacional'), 'Arley David Fernandez Cervantes');
$equipo->addPlayer($equipo->getTeam('Atlético Nacional'), 'Erick Yesid Bolaños Suarez');
$equipo->addPlayer($equipo->getTeam('América de Cali'), 'Oscar David González Vargas');


?>

<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Campeonato</title>
</head>
<body>

<?php
$navbar->getNavigation();
?>

<div class="container">
    <div class="row">

        <?php if (count($equipo->getTeams()) > 0): ?>

            <h1>Equipos</h1>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Equipos</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($equipo->getTeams() as $row): ?>
                    <tr>
                        <td><?php echo $row['nombre'] ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </br>
        <?php endif; ?>

        <?php if (count($equipo->getPlayers()) > 0): ?>

            <h1>Jugadores</h1>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Equipo</th>
                    <th>Jugador</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($equipo->getPlayers() as $row): ?>
                    <tr>
                        <td>
                            <?php
                            echo $equipo->getTeamForId($row['equipo'])['nombre'];
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $row['jugador']
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </br>
        <?php endif; ?>

        <?php if (count($equipo->getGroups()) > 0): ?>

            <h1>Grupos</h1>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Grupos</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0; ?>
                <?php foreach ($equipo->getGroups() as $row): ?>
                    <?php $i++; ?>
                    <tr>
                        <th>Grupo <?php echo $i; ?></th>
                    </tr>
                    <?php foreach ($row as $key => $value): ?>
                        <tr>
                            <td><?php
                                echo $value['nombre']
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
            </br>
        <?php endif; ?>

    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous">
</script>
</body>
</html>