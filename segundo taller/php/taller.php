<?php

class taller
{
    private $team;
    private $group;
    private $countTeam;
    private $countGroup;

    /**
     * taller constructor.
     */
    public function __construct()
    {
        $this->team = array();
        $this->group = array();

        $this->countTeam = 0;
        $this->countGroup = 0;
    }

    /**
     * @param $v
     */
    public function addTeam($v)
    {
        $this->countTeam ++;
        $this->team[$v] = $this->countTeam;
    }

    /**
     * @param $v
     */
    public function addGroup($v)
    {
        $this->countGroup++;
        $this->group[$v] = $this->countGroup;
    }


    /**
     * @return mixed
     */
    public function showTeams()
    {
        return print_r($this->team);
    }

    /**
     * @param $v
     */
    public function findTeam($v)
    {
        print_r($this->team[$v]['nombre']);
    }


    public function showCount()
    {
        echo $this->countTeam;
    }

    /**
     * @return mixed
     */
    public function showGroups()
    {
        return print_r($this->group);
    }

}