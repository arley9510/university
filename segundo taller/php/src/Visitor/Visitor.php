<?php

namespace Tree\Visitor;

use Tree\Node\NodeInterface;

/**
 * Visitor interface for Nodes
 *
 * @package    Tree
 * @author     Nicolò Martini <nicmartnic@gmail.com>
 */
interface Visitor
{
    /**
     * @param NodeInterface $node
     * @return mixed
     */
    public function visit(NodeInterface $node);
}