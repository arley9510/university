<?php

namespace Tree\Visitor;


use Tree\Node\NodeInterface;

/**
 * Class YieldVisitor
 *
 * @package Tree\Visitor
 */
class YieldVisitor implements Visitor
{
    /**
     * {@inheritdoc}
     */
    public function visit(NodeInterface $node)
    {
        if ($node->isLeaf()) {
            return [$node];
        }

        $yield = [];

        foreach ($node->getChildren() as $child) {
            $yield = array_merge($yield, $child->accept($this));
        }

        return $yield;
    }
} 