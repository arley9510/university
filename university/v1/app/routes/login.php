<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once('models/login/AuthClass.php');
require_once('models/login/LoginClass.php');

$auth = new AuthClass();
$login = new LoginClass();
$log = new LogClass();

$app->post('/api/login', function (Request $request, Response $response) use ($login, $auth, $log) {

    $email = $request->getParam('email');
    $password = $request->getParam('password');
    $device = $request->getParam('device');
    $grant_type = $request->getParam('grant_type');

    try {

        if ($grant_type === 'password') {

            $token = $login->login(strtolower($email), $password);

            if (isset($token) && $token !== null) {

                $login->saveSession($token, $device);

                echo '{"token": "' . $token . '" }';

                return $newResponse = $response->withStatus(201);
            } else {
                echo '{"error": "email o password equivocado"}';

                return $newResponse = $response->withStatus(422);
            }
        } else {
            echo '{"error": "email o password equivocado"}';

            return $newResponse = $response->withStatus(422);
        }
    } catch (PDOException $PDOException) {

        echo '{"error": ' . $PDOException->getMessage() . '}';
        $log->sendLog($PDOException);

        return $newResponse = $response->withStatus(500);
    }

});