<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$group = new GroupsClass();
$log = new LogClass();

/**
 *
 */
$app->post('/api/student-groups', function (Request $request, Response $response) use ($group, $log) {
    $token = $request->getParam('token');
    $device = $request->getParam('device');

    try {

        if ($token !== null && $device !== null) {

            $groups = $group->getGroups($token, $device);

            print_r($groups);

            return $newResponse = $response->withStatus(200);
        } else {

            echo '{"error": "acción invalida"}';

            return $newResponse = $response->withStatus(422);
        }

    } catch (PDOException $PDOException) {

        echo '{"error": ' . $PDOException->getMessage() . '}';
        $log->sendLog($PDOException);

        return $newResponse = $response->withStatus(500);
    }
});