<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once('models/logOut/LogOutClass.php');

$logOut = new LogOutClass();
$log = new LogClass();

$app->patch('/api/logout', function (Request $request, Response $response) use ($logOut, $log) {
    $token = $request->getParam('token');
    $device = $request->getParam('device');

    try {

        if ($token !== null && $device !== null) {

            $valid = $logOut->logOut($token, $device);

            if ($valid === true) {

                return $newResponse = $response->withStatus(204);
            } else {

                echo '{"error": "acción invalida"}';

                return $newResponse = $response->withStatus(422 );
            }
        } else {
            echo '{"error": "acción invalida"}';

            return $newResponse = $response->withStatus(422);
        }
    } catch (PDOException $PDOException) {

        echo '{"error": ' . $PDOException->getMessage() . '}';
        $log->sendLog($PDOException);

        return $newResponse = $response->withStatus(500);
    }
});