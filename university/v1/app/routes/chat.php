<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once('models/chat/ChatClass.php');
require_once('models/login/LoginClass.php');

$log = new LogClass();
$chat = new ChatClass();

$app->post('/api/chat', function (Request $request, Response $response) use ($chat, $log) {

    $token = $request->getParam('token');
    $device = $request->getParam('device');
    $from = $request->getParam('from');
    $to = $request->getParam('to');

    try {
        if ($token !== null && $device !== null) {
            $chatResponse = $chat->chatRoom($token, $device, $from, $to);

            if ($chatResponse !== false) {

                echo $chatResponse;

                return $response->withStatus(201);
            } else {

                echo '{"error": "acción invalida"}';

                return $response->withStatus(422);
            }
        } else {
            echo '{"error": "acción invalida"}';

            return $response->withStatus(422);
        }
    } catch (PDOException $PDOException) {

        $log->sendLog($PDOException);
    }
});