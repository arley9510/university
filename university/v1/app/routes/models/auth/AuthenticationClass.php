<?php

final class AuthenticationClass
{

    private $auth;
    private $log;

    public function __construct()
    {
        $this->auth = new AuthClass();
        $this->log = new LogClass();
    }

    /**
     * @param $token
     * @param $device
     * @return bool
     */
    public function validateToken($token, $device)
    {
        $data = $this->auth->decodeToken($token);

        if (!$this->validateExpiration($data['expire'])) {
            if ($this->validateDevice($token, $device) !== null) {

                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }

    /**
     * @param $expire
     * @return bool
     */
    public function validateExpiration($expire)
    {
        $date = new DateTime();

        return ($date->getTimestamp() > $expire);
    }

    /**
     * @param $token
     * @param $device
     * @return mixed|null
     */
    public function validateDevice($token, $device)
    {

        $sql = "SELECT D.person_person_id FROM auth A INNER JOIN device D ON A.device_device = D.device WHERE A.token = '$token' AND A.device_device = '$device'";

        $id = null;

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);

            while (($row = $stmt->fetch(PDO::FETCH_ASSOC)) !== false) {
                $id = $row;
            }
            $db = null;

            return $id;
        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }

    }
}