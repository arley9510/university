<?php

final class LogOutClass
{

    private $log;

    public function __construct()
    {
        $this->log = new LogClass();
    }

    /**
     * @param $token
     * @param $device
     * @return bool|null
     */
    public function logOut($token, $device)
    {
        $id = $this->validateToken($token, $device);

        if ($id !== null) {
            $isLogged = $this->validateSession($id);

            if ($isLogged === "1") {

                $this->closeSession($id, $device);

                return true;
            } else {
                return null;
            }
        } else {

            return null;
        }
    }

    /**
     * @param $token
     * @param $device
     * @return mixed|null
     */
    public function validateToken($token, $device)
    {

        $sql = "SELECT D.person_person_id FROM auth A INNER JOIN device D ON A.device_device = D.device WHERE A.token = '$token' AND A.device_device = '$device'";
        $id = null;

        try {
            $db = new db();
            $db = $db->connect();
            $stmt = $db->query($sql);

            while (($row = $stmt->fetch(PDO::FETCH_COLUMN)) !== false) {
                $id = $row;
            }
            $db = null;

            return $id;

        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public function validateSession($id)
    {
        $sql = "select is_logged FROM user WHERE person_person_id = $id";
        $isLogged = null;

        try {
            $db = new db();
            $db = $db->connect();
            $stmt = $db->query($sql);

            while (($row = $stmt->fetch(PDO::FETCH_COLUMN)) !== false) {
                $isLogged = $row;
            }
            $db = null;

            return $isLogged;

        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }

    public function closeSession($id, $device)
    {
        $this->updateUser($id);
        $this->deleteAuth($device);
        $this->deleteDevice($id);

    }

    public function updateUser($id)
    {
        $sql = "UPDATE user SET is_logged = 0 WHERE person_person_id = $id";

        try {

            $db = new db();
            $db = $db->connect();
            $stmt = $db->query($sql);
            $stmt->execute();
            $db = null;

        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }

    public function deleteDevice($id)
    {
        $sql = "DELETE FROM device WHERE person_person_id = $id";

        try {

            $db = new db();
            $db = $db->connect();
            $stmt = $db->query($sql);
            $stmt->execute();
            $db = null;

        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }

    public function deleteAuth($device)
    {
        $sql = "DELETE FROM `auth` WHERE device_device = '$device'";

        try {

            $db = new db();
            $db = $db->connect();
            $stmt = $db->query($sql);
            $stmt->execute();
            $db = null;

        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }
}