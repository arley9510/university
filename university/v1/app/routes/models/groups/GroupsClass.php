<?php

final class GroupsClass
{
    private $authentication;
    private $auth;
    private $log;

    public function __construct()
    {
        $this->authentication = new AuthenticationClass();
        $this->auth = new AuthClass();
        $this->log = new LogClass();
    }

    /**
     * @param $token
     * @param $device
     * @return null|string
     */
    public function getGroups($token, $device)
    {

        if ($this->authentication->validateToken($token, $device)) {

            return $this->groups($token);

        } else {

            return null;
        }

    }


    /**
     * @param $token
     * @return string
     */
    public function groups($token)
    {
        $email = $this->auth->decodeToken($token);
        $id = $this->getId($email['email']);

        if ($id !== null) {
            $code = $this->getCode($id);

            return $this->getGroupsForStudent($code);
        }
    }

    /**
     * @param $data
     * @return string
     */
    public function getGroupsForStudent($data)
    {
        $code = array_shift($data);

        $sql = "SELECT S.signature_id, S.name, T.teacher_code FROM signature S
                INNER JOIN teacher_has_signature TS
                ON S.signature_id = TS.signature_signature_id
                INNER JOIN teacher T
                ON TS.teacher_teacher_code = T.teacher_code
                LEFT JOIN inscription 
                I ON TS.id = I.teacher_has_signature_id
                WHERE I.student_student_code = '$code->student_code' ORDER BY S.name";

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);
            $groups = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return json_encode($groups);
        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $email
     * @return array
     */
    public function getId($email)
    {
        $sql = "SELECT person_person_id from user WHERE email = '$email'";

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);
            $id = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return $id;
        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function getCode($data)
    {
        $id = array_shift($data);

        $sql = "SELECT student_code FROM student WHERE 	person_person_id = $id->person_person_id";

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);
            $id = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return $id;
        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }
}