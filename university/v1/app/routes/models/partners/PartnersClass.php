<?php

final class PartnersClass
{

    private $authentication;
    private $auth;
    private $log;

    public function __construct()
    {
        $this->authentication = new AuthenticationClass();
        $this->auth = new AuthClass();
        $this->log = new LogClass();
    }

    public function getPartners($token, $device)
    {
        if ($this->authentication->validateToken($token, $device)) {

            return $this->partners($token);

        } else {

            return null;
        }
    }

    public function partners($token)
    {
        $email = $this->auth->decodeToken($token);
        $code = $this->getCode($email['email']);

        if ($code !== null) {

            return $this->getPartnersForStudent($code);
        }
    }

    /**
     * @param $email
     * @return array
     */
    public function getCode($email)
    {
        $sql = "SELECT DISTINCT S.student_code from user U
                INNER JOIN student S
                ON U.person_person_id = S.person_person_id
                WHERE U.email =  '$email'";

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);
            $code = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return $code;
        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $data
     * @return string
     */
    public function getPartnersForStudent($data)
    {
        $code = array_shift($data);

        $sql = "SELECT DISTINCT S.student_code, CONCAT(P.first_name, ' ', P.last_name) AS name, P.phone FROM person P
                INNER JOIN student S
                ON P.person_id = S.person_person_id
                INNER JOIN inscription I
                ON S.student_code = I.student_student_code
                WHERE I.teacher_has_signature_id IN (
                  SELECT DISTINCT S.signature_id FROM signature S
                  INNER JOIN teacher_has_signature TS
                  ON S.signature_id = TS.signature_signature_id
                  INNER JOIN inscription I ON TS.id = I.teacher_has_signature_id
                  WHERE I.student_student_code = '$code->student_code'
                ) AND S.student_code != '$code->student_code'";

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);
            $groups = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return json_encode($groups);
        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }
}