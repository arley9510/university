<?php

require_once('AuthClass.php');

final class LoginClass
{

    private $log;

    public function __construct()
    {
        $this->log = new LogClass();
    }

    /**
     * @param $email
     * @param $password
     * @return null|string
     */
    public function login($email, $password)
    {
        $auth = new AuthClass();

        if ($email !== null && $email !== null) {

            $id = $this->validateEmail($email)['person_person_id'];

            if ($id !== null) {

                $data = $this->validatePassword($email, $password, $id);

                if (count($data) > 0) {

                    return $auth->newToken($data);
                } else {

                    return null;
                }
            }
        } else {

            return null;
        }
    }

    /**
     * @param $email
     * @return mixed|null
     */
    public function validateEmail($email)
    {
        $sql = "SELECT person_person_id FROM user WHERE email= '$email' AND is_logged = 0";
        $id = null;

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);

            while (($row = $stmt->fetch(PDO::FETCH_ASSOC)) !== false) {
                $id = $row;
            }
            $db = null;

            return $id;
        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $email
     * @param $password
     * @param $id
     * @return array|bool
     */
    public function validatePassword($email, $password, $id)
    {
        if (is_numeric($id)) {
            $sql = "SELECT P.first_name, P.last_name, P.phone, P.venue_venue_id, U.email FROM person P INNER JOIN user U ON P.person_id = U.person_person_id WHERE P.person_id = $id AND U.email = '$email ' AND U.password = '$password'";

            try {
                $db = new db();
                $db = $db->connect();
                $stmt = $db->query($sql);
                $user = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;

                if (is_array($user)) {

                    return ($user);
                } else {

                    return null;
                }

            } catch (PDOException $PDOException) {
                echo $PDOException;

                $this->log->sendLog($PDOException);
            }
        } else {

            return null;
        }
    }

    /**
     * @param $token
     * @param $device
     */
    public function saveSession($token, $device)
    {
        try {
            $auth = new AuthClass();
            $information = $auth->decodeToken($token);

            $email = $information['email'];
            $id = $this->validateEmail($email)['person_person_id'];

            $this->updateUser($id);
            $this->saveDevice($id, $device);
            $this->saveAuth($token, $device);

        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $id
     */
    public function updateUser($id)
    {
        $sql = "UPDATE user SET is_logged = 1 WHERE person_person_id = $id";

        try {
            $db = new db();
            $db = $db->connect();
            $stmt = $db->prepare($sql);
            $stmt->execute();

        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $id
     * @param $device
     */
    public function saveDevice($id, $device)
    {
        $date = new DateTime('now', new DateTimeZone('America/Bogota'));
        $sql = "INSERT INTO device VALUES(:device, :create_at, :person_person_id)";
        $date = $date->getTimestamp();

        try {

            $db = new db();
            $db = $db->connect();
            $stmt = $db->prepare($sql);

            $stmt->bindParam('device', $device);
            $stmt->bindParam('create_at', $date);
            $stmt->bindParam('person_person_id', $id);

            $stmt->execute();

        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $device
     * @param $token
     */
    public function saveAuth($token, $device)
    {
        $auth = new AuthClass();
        $information = $auth->decodeToken($token);

        $create_at = $information['valid at'];
        $expire_at = $information['expire'];

        $sql = "INSERT INTO auth VALUES(:token, :token_create_at, :token_expires, :device_device)";

        try {

            $db = new db();
            $db = $db->connect();
            $stmt = $db->prepare($sql);

            $stmt->bindParam('token',$token);
            $stmt->bindParam('token_create_at',$create_at);
            $stmt->bindParam('token_expires',$expire_at);
            $stmt->bindParam('device_device',$device);

            $stmt->execute();

        } catch (PDOException $PDOException) {
            echo $PDOException;

            $this->log->sendLog($PDOException);
        }
    }

}