<?php

use \Firebase\JWT\JWT;

final class AuthClass
{
    private $key = "example_key";

    /**
     * @param $data
     * @return null|string
     */
    public function newToken($data)
    {
        if (isset($data)) {
            $data = array_shift($data);

            $valid = $this->getTime();
            $expire = strtotime("+1 week");

            $token = array(
                "iss" => "http://usbbog.edu.com",
                "first name" => $data->first_name,
                "last name" => $data->last_name,
                "email" => $data->email,
                "phone" => $data->phone,
                "venue" => $data->venue_venue_id,
                "valid at" => $valid->getTimestamp(),
                "expire" => $expire,
            );

            $jwt = JWT::encode($token, $this->key);

            return $jwt;
        } else {

            return null;
        }
    }

    /**
     * @param $jwt
     * @return array
     */
    public function decodeToken($jwt)
    {
        $decoded = JWT::decode($jwt, $this->key, array('HS256'));

        return $token = (array) $decoded;

    }

    /**
     * @return DateTime
     */
    public function getTime()
    {
        $date = new DateTime('now', new DateTimeZone('America/Bogota'));

        return $date;

    }

}