<?php

final class CityClass
{
    /**
     * @param $sql
     * @return null|string
     */
    public function query($sql)
    {
        if (is_string($sql) && $sql !== null) {

            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);
            $city = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return json_encode($city, JSON_UNESCAPED_UNICODE);
        } else {
            return null;
        }
    }

    /**
     * @param $name
     * @param $code
     * @param $state_state_id
     * @param $sql
     * @return null|string
     */
    public function queryAdvance($name, $code, $state_state_id, $sql)
    {
        if ($name !== null &&
            $code !== null &&
            $state_state_id !== null) {

            $db = new db();
            $db = $db->connect();
            $stmt = $db->prepare($sql);

            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':code', $code);
            $stmt->bindParam(':state_state_id', $state_state_id);

            $stmt->execute();

            $db = null;

            return '{"notice": {"text": "request susses"}';
        } else {
            return null;
        }
    }

    /**
     * @param $sql
     * @return null|string
     */
    public function queryDelete($sql)
    {
        if (is_string($sql) && $sql !== null) {

            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);
            $stmt->execute();
            $db = null;

            return '{"notice": {"text": "element delete"}';
        } else {
            return null;
        }
    }

}