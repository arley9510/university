<?php

final class ChatClass
{
    private $authentication;
    private $auth;
    private $log;

    public function __construct()
    {
        $this->authentication = new AuthenticationClass();
        $this->auth = new AuthClass();
        $this->log = new LogClass();
    }

    /**
     * @param $token
     * @param $device
     * @param $from
     * @param $to
     * @return null
     */
    public function chatRoom($token, $device, $from, $to)
    {
        if ($this->authentication->validateToken($token, $device)) {

            return $this->getChat($from, $to);

        } else {

            return false;
        }
    }

    /**
     * @param $from
     * @param $to
     * @return bool|string
     */
    public function getChat($from, $to)
    {
        if ($from !== null && $to !== null) {

            $exist = $this->getChatIfExist($from, $to);

            if ($exist !== false) {

                return $exist;
            } else {

                $sql = "INSERT INTO chats(user_from, user_to, chat_id) VALUES (:user_from, :uer_to, :chat)";

                try {
                    $db = new db();
                    $db = $db->connect();
                    $stmt = $db->prepare($sql);

                    $chat = $from . '-' . $to . '-' . time();

                    $stmt->bindParam(':user_from', $from);
                    $stmt->bindParam(':uer_to', $to);
                    $stmt->bindParam(':chat', $chat);

                    if ($stmt->execute() === true) {
                        return $this->getChat($from, $to);
                    } else {

                        return false;
                    }

                    $db = null;
                } catch (PDOException $PDOException) {

                    $this->log->sendLog($PDOException);
                }
            }
        } else {

            return false;
        }
    }

    /**
     * @param $from
     * @param $to
     * @return string
     */
    public function getChatIfExist($from, $to)
    {
        $sql = "SELECT chat_id FROM chats WHERE (user_from = '$from' AND user_to = '$to') OR (user_to = '$to' AND user_from = '$from')";

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);
            $chat = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;

            return count($chat) > 0 ? json_encode($chat) : false;
        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }
}