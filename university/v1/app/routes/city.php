<?php

require_once('models/city/CityClass.php');

$city = new CityClass();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Create City
$app->get('/api/city', function (Request $request, Response $response) use ($city) {

    $sql = 'SELECT * FROM city';

    try {
        $res = $city->query($sql);
        if ($res !== null) {
            echo $res;

            return $newResponse = $response->withStatus(200);
        } else {
            echo '{"error": {"text": "Unprocessable Entity"}';

            return $newResponse = $response->withStatus(422);
        }

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';

        return $newResponse = $response->withStatus(500);
    }
});

// Get Single City
$app->get('/api/city/{id:\d+}', function (Request $request, Response $response) use ($city) {

    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM city WHERE city_id = $id";

    try {
        $res = $city->query($sql);
        if ($res !== null) {
            echo $res;

            return $newResponse = $response->withStatus(200);
        } else {
            echo '{"error": {"text": "Unprocessable Entity"}';

            return $newResponse = $response->withStatus(422);
        }

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';

        return $newResponse = $response->withStatus(500);
    }
});

// Add City
$app->post('/api/city/add', function (Request $request, Response $response) use ($city) {
    $name = $request->getParam('name');
    $code = $request->getParam('code');
    $state_state_id = $request->getParam('state_state_id');

    $sql = "INSERT INTO city (name, code, state_state_id) VALUES (:name, :code, :state_state_id)";

    try {
        $res = $city->queryAdvance($name, $code, $state_state_id, $sql);
        if ($res !== null) {
            echo $res;

            return $newResponse = $response->withStatus(201);
        } else {
            echo '{"error": {"text": "Unprocessable Entity"}';

            return $newResponse = $response->withStatus(422);
        }

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';

        return $newResponse = $response->withStatus(500);
    }
});

// Update City
$app->put('/api/city/update/{id:\d+}', function (Request $request, Response $response) use ($city) {
    $id = $request->getAttribute('id');
    $name = $request->getParam('name');
    $code = $request->getParam('code');
    $state_state_id = $request->getParam('state_state_id');

    $sql = "UPDATE city SET name = :name, name = :name, state_state_id = :state_state_id WHERE city_id = $id";

    try {
        $res = $city->queryAdvance($name, $code, $state_state_id, $sql);
        if ($res !== null) {
            echo $res;

            return $newResponse = $response->withStatus(201);
        } else {
            echo '{"error": {"text": "Unprocessable Entity"}';

            return $newResponse = $response->withStatus(422);
        }

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';
        return $newResponse = $response->withStatus(500);
    }
});

// Delete City
$app->delete('/api/city/delete/{id:\d+}', function (Request $request, Response $response) use ($city) {
    $id = $request->getAttribute('id');

    $sql = "DELETE FROM city WHERE city_id = $id";

    try {
        $res = $city->queryDelete($sql);
        if ($res !== null) {
            echo $res;

            return $newResponse = $response->withStatus(200);
        } else {
            echo '{"error": {"text": "Unprocessable Entity"}';

            return $newResponse = $response->withStatus(422);
        }

    } catch (PDOException $e) {
        echo '{"error": {"text": ' . $e->getMessage() . '}';

        return $newResponse = $response->withStatus(500);
    }
});