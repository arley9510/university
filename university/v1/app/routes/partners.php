<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$log = new LogClass();
$partner = new PartnersClass();
$newResponse = null;

$app->post('/api/student-partners', function (Request $request, Response $response) use ($partner, $log) {
    $token = $request->getParam('token');
    $device = $request->getParam('device');

    try {

        if ($token !== null && $device !== null) {

            $groups = $partner->getPartners($token, $device);

            print_r($groups);

            return $this->newResponse = $response->withStatus(200);
        } else {

            echo '{"error": "acción invalida"}';

            return $this->newResponse = $response->withStatus(422);
        }

    } catch (PDOException $PDOException) {

        echo '{"error": ' . $PDOException->getMessage() . '}';
        $log->sendLog($PDOException);

        return $this->newResponse = $response->withStatus(500);
    }
});