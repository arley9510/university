<?php

class LogClass
{
    private $client;

    public function __construct()
    {
        try {
            $this->client = (new Raven_Client('https://ceb0a106eb9b4ea98a00cd41b0f49f3c:fbc83f3c8aad414e808bce77a630f44b@sentry.io/1116909'))->install();
        } catch (Raven_Exception $e) {
        }
    }

    public function sendLog($event)
    {

        $this->client->captureException($event);

    }

}