<?php

require_once('../vendor/sentry/sentry/lib/Raven/Autoloader.php');

require '../app/routes/models/auth/AuthenticationClass.php';
require '../app/routes/models/partners/PartnersClass.php';
require '../app/routes/models/groups/GroupsClass.php';
require '../app/routes/models/login/AuthClass.php';
require '../app/log/LogClass.php';
require '../vendor/autoload.php';
require '../app/config/db.php';
Raven_Autoloader::register();

$app = new \Slim\App;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

[
    require '../app/routes/chat.php',
    require '../app/routes/city.php',
    require '../app/routes/login.php',
    require '../app/routes/logOut.php',
    require '../app/routes/groups.php',
    require '../app/routes/partners.php',
];

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', '*');
});

try {
    $app->run();
} catch (\Slim\Exception\MethodNotAllowedException $e) {
    $log->sendLog($e);
} catch (\Slim\Exception\NotFoundException $e) {
    $log->sendLog($e);
} catch (Exception $e) {
    $log->sendLog($e);
}
