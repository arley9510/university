<?php

require_once('src/login/CheckLogin.php');

// valida la session y redirige
$check = new CheckLogin();
$check->check();

?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo4.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Home</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/gallery/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"
            integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"
            integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c"
            crossorigin="anonymous"></script>


</head>
<body>

<!-- barra de navegacion -->
<section class="testimonials3 cid-qK9Msr0Afv" id="testimonials3-m">

    <div class="container">
        <div class="media-container-row">
            <div class="media-content px-3 align-self-center mbr-white py-2">
                <p class="mbr-author-name pt-4 mb-2 mbr-fonts-style display-7"><?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name'] ?></p>
            </div>

            <div class="mbr-figure pl-lg-5" style="width: 20%;">
                <img src="assets/images/face5.jpg">
            </div>

            <div class="navbar-brand">
                <a href="src/login/LogOut.php">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- secion de contenido -->
<section class="testimonials4 cid-qK9N4hVlHj" id="testimonials4-n">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);">
    </div>
    <div class="container">

        <div class="col-md-10 testimonials-container">

            <div class="testimonials-item">
                <div class="user row">
                    <div class="col-lg-3 col-md-4">
                        <div class="user_image">
                            <img src="assets/images/face3.jpg">
                        </div>
                    </div>
                    <div class="testimonials-caption col-lg-9 col-md-8">
                        <div class="user_text ">
                            <p class="mbr-fonts-style  display-7">
                                <em>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae nostrum, quos
                                    voluptas fugiat blanditiis, temporibus expedita cumque doloribus ea, officiis
                                    consequuntur repellat minus ad veritatis? Facere similique accusamus, accusantium
                                    sunt!"</em>
                            </p>
                        </div>
                        <div class="user_name mbr-bold mbr-fonts-style align-left pt-3 display-7">
                            Helen
                        </div>
                        <div class="user_desk mbr-light mbr-fonts-style align-left pt-2 display-7">
                            DESIGNER
                        </div>
                    </div>
                </div>
            </div>
            <div class="testimonials-item">
                <div class="user row">
                    <div class="col-lg-3 col-md-4">
                        <div class="user_image">
                            <img src="assets/images/face1.jpg">
                        </div>
                    </div>
                    <div class="testimonials-caption col-lg-9 col-md-8">
                        <div class="user_text">
                            <p class="mbr-fonts-style  display-7">
                                <em>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae nostrum, quos
                                    voluptas fugiat blanditiis, temporibus expedita cumque doloribus ea, officiis
                                    consequuntur repellat minus ad veritatis? Facere similique accusamus, accusantium
                                    sunt!"</em>
                            </p>
                        </div>
                        <div class="user_name mbr-bold mbr-fonts-style align-left pt-3 display-7">
                            Linda
                        </div>
                        <div class="user_desk mbr-light mbr-fonts-style align-left pt-2 display-7">
                            DEVELOPER
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- seccion de galeria -->
<section class="mbr-gallery mbr-slider-carousel cid-qLFFG6d40s" id="gallery3-v">


    <div>
        <div>
            <!-- Filter -->
            <!-- Gallery -->
            <div class="mbr-gallery-row">
                <div class="mbr-gallery-layout-default">
                    <div>
                        <div>
                            <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                 data-tags="Awesome">
                                <div href="#lb-gallery3-v" data-slide-to="0" data-toggle="modal"><img
                                            src="assets/images/gallery00.jpg" alt=""><span class="icon-focus"></span>
                                </div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                 data-tags="Responsive">
                                <div href="#lb-gallery3-v" data-slide-to="1" data-toggle="modal"><img
                                            src="assets/images/gallery01.jpg" alt=""><span class="icon-focus"></span>
                                </div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                 data-tags="Creative">
                                <div href="#lb-gallery3-v" data-slide-to="2" data-toggle="modal"><img
                                            src="assets/images/gallery02.jpg" alt=""><span class="icon-focus"></span>
                                </div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                 data-tags="Animated">
                                <div href="#lb-gallery3-v" data-slide-to="3" data-toggle="modal"><img
                                            src="assets/images/gallery03.jpg" alt=""><span class="icon-focus"></span>
                                </div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                 data-tags="Awesome">
                                <div href="#lb-gallery3-v" data-slide-to="4" data-toggle="modal"><img
                                            src="assets/images/gallery04.jpg" alt=""><span class="icon-focus"></span>
                                </div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                 data-tags="Awesome">
                                <div href="#lb-gallery3-v" data-slide-to="5" data-toggle="modal"><img
                                            src="assets/images/gallery05.jpg" alt=""><span class="icon-focus"></span>
                                </div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                 data-tags="Responsive">
                                <div href="#lb-gallery3-v" data-slide-to="6" data-toggle="modal"><img
                                            src="assets/images/gallery06.jpg" alt=""><span class="icon-focus"></span>
                                </div>
                            </div>
                            <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false"
                                 data-tags="Animated">
                                <div href="#lb-gallery3-v" data-slide-to="7" data-toggle="modal"><img
                                            src="assets/images/gallery07.jpg" alt=""><span class="icon-focus"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Lightbox -->
            <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1"
                 data-keyboard="true" data-interval="false" id="lb-gallery3-v">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="carousel-inner">
                                <div class="carousel-item active"><img src="assets/images/gallery00.jpg" alt=""></div>
                                <div class="carousel-item"><img src="assets/images/gallery01.jpg" alt=""></div>
                                <div class="carousel-item"><img src="assets/images/gallery02.jpg" alt=""></div>
                                <div class="carousel-item"><img src="assets/images/gallery03.jpg" alt=""></div>
                                <div class="carousel-item"><img src="assets/images/gallery04.jpg" alt=""></div>
                                <div class="carousel-item"><img src="assets/images/gallery05.jpg" alt=""></div>
                                <div class="carousel-item"><img src="assets/images/gallery06.jpg" alt=""></div>
                                <div class="carousel-item"><img src="assets/images/gallery07.jpg" alt=""></div>
                            </div>
                            <a class="carousel-control carousel-control-prev" role="button" data-slide="prev"
                               href="#lb-gallery3-v"><span class="mbri-left mbr-iconfont"
                                                           aria-hidden="true"></span><span
                                        class="sr-only">Previous</span></a><a
                                    class="carousel-control carousel-control-next"
                                    role="button" data-slide="next"
                                    href="#lb-gallery3-v"><span
                                        class="mbri-right mbr-iconfont" aria-hidden="true"></span><span
                                        class="sr-only">Next</span></a><a class="close" href="#" role="button"
                                                                          data-dismiss="modal"><span
                                        class="sr-only">Close</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/popper/popper.min.js"></script>
<script src="assets/tether/tether.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/smoothscroll/smooth-scroll.js"></script>
<script src="assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js"></script>
<script src="assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
<script src="assets/masonry/masonry.pkgd.min.js"></script>
<script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="assets/theme/js/script.js"></script>
<script src="assets/gallery/player.min.js"></script>
<script src="assets/gallery/script.js"></script>
<script src="assets/slidervideo/script.js"></script>

</body>
</html>