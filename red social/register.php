<?php

require_once('src/NavBar.php');

// inicaliza la barra de navegacion
$navigation = new NavBar();

?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo4.png" type="image/x-icon">
    <title>Register</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">

</head>
<body>

<!-- barra de navegacion -->
<section class="menu cid-qLFDiK44OQ" id="menu1-t">

    <!-- instancia a la barra de navegacion -->
    <?php
    $navigation->getNavigationBar();
    ?>

</section>

<!-- imagen de fondo -->
<section class="engine"><a href="">how to design own website</a></section>
<section class="header13 cid-qLFDFUbRIY mbr-fullscreen mbr-parallax-background" id="header13-u">

    <!-- opacidad de la imagen -->
    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(35, 35, 35);">
    </div>

    <!-- ccntainer responsive -->
    <div class="container">

        <!-- titulo -->
        <h1 class="mbr-section-title align-center pb-3 mbr-white mbr-bold mbr-fonts-style display-1">
            INTRO WITH POPUP VIDEO
        </h1>

        <!-- subtitulo -->
        <h3 class="mbr-section-subtitle mbr-fonts-style display-5">
            Full-screen intro with background image, color, overlay, icons, popup video and subscribe form
        </h3>

        <!-- contenedor del form -->
        <div class="container mt-5 pt-5 pb-5">
            <div class="media-container-column" data-form-type="formoid">

                <!-- formulario de registro -->
                <form class="form-inline" action="src/register/register.php" method="POST">

                    <div class="mbr-form">
                        <input type="text" class="form-control px-3" name="name" required
                               placeholder="Name">
                    </div>

                    <div class="form-group">
                        <input type="email" class="form-control" name="email" required
                               placeholder="Email">
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" name="password" required
                               placeholder="Password">
                    </div>

                    <div class="buttons-wrap">
                        <input class="btn btn-primary display-4" type="submit" value="Register">
                    </div>

                </form>
            </div>
        </div>
    </div>

</section>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/popper/popper.min.js"></script>
<script src="assets/tether/tether.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/smoothscroll/smooth-scroll.js"></script>
<script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
<script src="assets/playervimeo/vimeo_player.js"></script>
<script src="assets/parallax/jarallax.min.js"></script>
<script src="assets/dropdown/js/script.min.js"></script>
<script src="assets/theme/js/script.js"></script>
<script src="assets/formoid/formoid.min.js"></script>

</body>
</html>