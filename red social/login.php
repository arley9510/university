<?php

require_once('src/NavBar.php');

// inicializa la barra de navegacion
$navigation = new NavBar();

?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo4.png" type="image/x-icon">
    <title>Log in</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">

</head>
<body>

<!-- barra de navegacion -->
<section class="menu cid-qLFDiK44OQ" id="menu1-s">

    <!-- instancia a la barra de navegacion -->
    <?php
    $navigation->getNavigationBar();
    ?>

</section>


<!-- imagen de fondo -->
<section class="cid-qLFCfdah66 mbr-fullscreen mbr-parallax-background" id="header15-r">

    <!-- opacidad de la imagen -->
    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(7, 59, 76);"></div>

    <!-- ccntainer responsive -->
    <div class="container align-right">
        <div class="row">
            <div class="mbr-white col-lg-8 col-md-7 content-container">

                <!-- titulo -->
                <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">
                    INTRO WITH FORM
                </h1>

                <!-- parrafo -->
                <p class="mbr-text pb-3 mbr-fonts-style display-5">
                    Click any text to edit or style it. Select text to insert a link. Click blue "Gear" icon in the top
                    right corner to hide/show text, title and change the block or form background. Click red "+" in the
                    bottom right corner to add a new block. Use the top left menu to create new pages, sites and add
                    themes.
                </p>
            </div>

            <!-- contenedor responsive -->
            <div class="col-lg-4 col-md-5">
                <!-- contenedor del form -->
                <div class="form-container">
                    <div class="media-container-column">

                        <form class="mbr-form" action="src/login/login.php" method="POST">

                            <div class="form-group">
                                <input type="email" class="form-control" name="email" required
                                       placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" required
                                       placeholder="Password">
                            </div>

                            <input type="submit" class="btn btn-secondary" value="Log in"/>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/popper/popper.min.js"></script>
<script src="assets/tether/tether.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/dropdown/js/script.min.js"></script>
<script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
<script src="assets/parallax/jarallax.min.js"></script>
<script src="assets/smoothscroll/smooth-scroll.js"></script>
<script src="assets/theme/js/script.js"></script>
<script src="assets/formoid/formoid.min.js"></script>

</body>
</html>