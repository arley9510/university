<?php

require_once('../log/LogClass.php');

class UserClass
{
    private $log;

    public function __construct()
    {
        $this->log = new Log();
    }

    /**
     * @param $value
     * @return array|null
     */
    public function searchFriend($value)
    {
        if ($value !== null) {

            $sql = "SELECT * FROM friends where name = '$value' OR last_name = '$value' OR phone = '$value'";

            try {
                $db = new db();
                $db = $db->connect();

                $stmt = $db->query($sql);
                $friends = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;

                return $friends;

            } catch (PDOException $PDOException) {

                $this->log->sendLog($PDOException);
            }
        } else {

            return null;
        }
    }

    /**
     * @param $id
     * @param $user
     * @return bool
     */
    public function SendFriendRequestDestination($id, $user)
    {
        if ($user !== null) {

            $sql = "INSERT INTO friend_Request  VALUES(:from, :for, :create_at)";

            try {

                $db = new db();
                $db = $db->connect();
                $stmt = $db->prepare($sql);

                $date = new DateTime();
                $date = $date->getTimestamp();

                $stmt->bindParam(':from', $id);
                $stmt->bindParam(':for', $user);
                $stmt->bindParam(':create_at', $date);

                if ($stmt->execute()) {

                    return true;
                } else {

                    return false;
                }

                $db = null;
            } catch (PDOException $PDOException) {

                $this->log->sendLog($PDOException);
            }
        }
    }

    /**
     * @param $request
     * @param $response
     * @return bool|null
     */
    public function RespondFriendRequest($request, $response)
    {
        if ($request !== null && $response !== null) {

            $date = new DateTime();
            $date = $date->getTimestamp();

            $sql = "UPDATE friend_Request SET status = $response, updated_at = $date  WHERE id = $request";

            try {
                $db = new db();
                $db = $db->connect();
                $stmt = $db->prepare($sql);

                if ($stmt->execute()) {

                    return true;
                } else {

                    return false;
                }

                $db = null;
            } catch (PDOException $PDOException) {

                $this->log->sendLog($PDOException);
            }
        } else {

            return null;
        }
    }

    /**
     * @param $user
     * @return array|null
     */
    public function listFriends($user)
    {

        if ($user !== null) {

            $sql = "SELECT * FROM user_has_friends WHERE friend = '$user'";

            try {
                $db = new db();
                $db = $db->connect();

                $stmt = $db->query($sql);
                $friends = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;

                return $friends;
            } catch (PDOException $PDOException) {


                $this->log->sendLog($PDOException);
            }
        } else {

            return null;
        }

    }

    /**
     * @param $content
     * @param $isPublic
     * @param $linkTo
     * @return bool|null
     */
    public function newPost($content, $isPublic, $linkTo)
    {
        if ($content !== null && $isPublic !== null && $linkTo !== null) {

            $sql = "INSERT INTO post VALUES(:content, :isPublic, :linkTo, :createAt)";

            try {

                $db = new db();
                $db = $db->connect();
                $stmt = $db->prepare($sql);

                $date = new DateTime();
                $date = $date->getTimestamp();

                $stmt->bindParam(':content', $content);
                $stmt->bindParam(':isPublic', $isPublic);
                $stmt->bindParam(':linkTo', $linkTo);
                $stmt->bindParam(':createAt', $date);

                if ($stmt->execute()) {

                    return true;
                } else {

                    return false;
                }

                $db = null;
            } catch (PDOException $PDOException) {

                $this->log->sendLog($PDOException);
            }

        } else {

            return null;
        }
    }

    /**
     * @param $user
     * @param $friend
     * @return array|null
     */
    public function chat($user, $friend)
    {
        if ($user !== null && $friend !== null) {

            $sql = "SELECT * FROM chat_roms where user = '$user' AND friend = '$friend'";

            try {

                $db = new db();
                $db = $db->connect();

                $stmt = $db->query($sql);
                $chatRom = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;

                return $chatRom;
            } catch (PDOException $PDOException) {

                $this->log->sendLog($PDOException);
            }
        } else {

            return null;
        }
    }

    public function uploadImage($image)
    {

    }

    public function deleteImage()
    {

    }

    public function editProfile()
    {

    }

    /**
     * @param $email
     * @param $status
     * @return bool|null
     */
    public function recoveryPassword($email, $status)
    {
        if ($email !== null) {

            $sql = "INSERT INTO recovery_account values (:status, :email, :created_at)";

            try {

                $db = new db();
                $db = $db->connect();
                $stmt = $db->prepare($sql);

                $date = new DateTime();
                $date = $date->getTimestamp();

                $stmt->bindParam(':status',$status );
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':created_at', $date);

                if ($stmt->execute()) {

                    return true;
                } else {

                    return false;
                }

                $db = null;
            } catch (PDOException $PDOException) {

                $this->log->sendLog($PDOException);
            }
        } else {

            return null;
        }
    }


}