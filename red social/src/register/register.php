<?php

require_once('../db/db.php');
require_once ('../log/LogClass.php');

// si el metodo de entrada es post se valida

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    // si las variables tienen algun valor se procesan
    if (isset($email) || isset($password) || isset($name)) {

        $login = new Register();

        if($login->register($name, $email, $password)) {

            header('location: http://localhost:8080/university/red%20social/login.php');
        }

    } else {

        // sino se termina la sesion y se redirige al login
        session_destroy();
        header('location: http://localhost:8080/university/red%20social/register.php');
    }
}

/**
 * Class Register
 */
final class Register {

    private $log;

    public function __construct()
    {
        $this->log = new LogClass();
    }

    /**
     * @param $name
     * @param $email
     * @param $password
     */
    public function register($name, $email, $password)
    {
        $isValid = $this->validateEmail($email);

        if ($isValid === null) {

            $this->newUser($name, $email, $password);
        } else {

            echo "<scritp>alert('El email '$email' ya fue registrado.)</scritp>";
        }
    }

    /**
     * @param $email
     * @return int
     */
    public function validateEmail($email)
    {
        $sql = "SELECT person_person_id FROM user WHERE email = '$email'";
        $id = null;

        try {
            $db = new db1();
            $db = $db->connect();

            $stmt = $db->query($sql);

            while (($row = $stmt->fetch(PDO::FETCH_ASSOC)) !== false) {
                $id = $row;
            }
            $db = null;

            return intval($id);

        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $name
     * @param $email
     * @param $password
     * @return bool
     */
    public function newUser($name, $email, $password)
    {
        $sql = "INSERT INTO person VALUES(:name)";
        $id  = "SELECT person_id FROM person WHERE name = '$name'";
        $insertUser = "INSERT INTO user VALUES(:person_person_id, :email, :password)";

        try {

            $db = new db1();
            $db = $db->connect();

            $stmt = $db->prepare($sql);
            $stmt->execute();

            $stmt2 = $db->query($id);
            $stmt2->execute();

            $stmt3 = $db->query($insertUser);
            $stmt3->bindParam('person_person_id', $id);
            $stmt3->bindParam('email', $email);
            $stmt3->bindParam('password', $password);
            $stmt3->execute();

            return true;

        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }
}