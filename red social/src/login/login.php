<?php

require_once('../db/db.php');
require_once ('../log/LogClass.php');

// si el metodo de entrada es post se valida

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $email = $_POST['email'];
    $password = $_POST['password'];

    // si las variables tienen algun valor se procesan
    if (isset($email) || isset($password)) {

            $login = new Login();
            $login->login($email, $password);
    } else {

        // sino se termina la sesion y se redirige al login
        session_destroy();
        header('location: http://localhost:8080/university/red%20social/login.php');
    }

}

final class Login
{
    private $log;

    public function __construct()
    {
        $this->log = new Log();
    }

    /**
     * @param $email
     * @param $password
     *
     * valida atravez de las funciones validateEmail y validatePassword las credenciales del usuario
     * de ser validas redirige al home de lo contrario redirige al login
     */
    public function login($email, $password)
    {
        $id = $this->validateEmail($email);

        if ($id !== null) {
            $data = $this->validatePassword($email, $password, $id);

            if (count($data) > 0) {

                session_start();

                $data = array_shift($data);

                $_SESSION['first_name']     = $data->first_name;
                $_SESSION['last_name']      = $data->last_name;
                $_SESSION['phone']          = $data->phone;
                $_SESSION['email']          = $data->email;

                header('location: http://localhost:8080/university/red%20social/index.php');
            } else {
                session_destroy();
                header('location: http://localhost:8080/university/red%20social/login.php');
            }
        }
    }

    /**
     * @param $email
     * @return int
     *
     * valida el id de un usuario por su email registrado
     * retornando un id
     */
    private function validateEmail($email)
    {
        $sql = "SELECT person_person_id FROM user WHERE email = '$email'";
        $id = null;

        try {
            $db = new db1();
            $db = $db->connect();

            $stmt = $db->query($sql);

            while (($row = $stmt->fetch(PDO::FETCH_ASSOC)) !== false) {
                $id = $row;
            }
            $db = null;

            return intval($id);

        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $email
     * @param $password
     * @param $id
     * @return array|null
     *
     * valida con el email password y el id la eistencia de un usuario y retorna sus datos en un array
     */
    private function validatePassword($email, $password, $id)
    {

        if (is_numeric($id)) {

            $sql = "SELECT P.first_name, P.last_name, P.phone, P.venue_venue_id, U.email FROM person P INNER JOIN user U ON P.person_id = U.person_person_id WHERE P.person_id = $id AND U.email = '$email ' AND U.password = '$password'";

            try {
                $db = new db1();
                $db = $db->connect();
                $stmt = $db->query($sql);
                $user = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;

                if (is_array($user)) {

                    return ($user);
                } else {

                    return null;
                }

            } catch (PDOException $PDOException) {

                $this->log->sendLog($PDOException);
            }
        } else {

            return null;
        }

    }
}