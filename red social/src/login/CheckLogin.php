<?php

final class CheckLogin
{

    public function __construct()
    {

    }

    // valida si una ssion esta abieta y si no redirige al login
    public function check()
    {
        session_start();

        $fName = $_SESSION['first_name'];
        $sName = $_SESSION['last_name'];
        $phone = $_SESSION['phone'];
        $email = $_SESSION['email'];

        if (!isset($fName) && !isset($sName) && !isset($phone) && !isset($email)) {
            session_destroy();
            header('location: http://localhost:8080/university/red%20social/login.php');
        }
    }

    // valida si una session ya esta abierta y redirige a la pagina de inicio
    public function checkStarted()
    {

            $fName = $_SESSION['first_name'];
            $sName = $_SESSION['last_name'];
            $phone = $_SESSION['phone'];
            $email = $_SESSION['email'];

            if (isset($fName) && isset($sName) && isset($phone) && isset($email)) {

                header('location: http://localhost:8080/university/red%20social/index.php');
            }
    }
}