<?php

class NavBar
{
    /**
     *  retorna la barra de navegación del login y registro
     */
    public function getNavigationBar()
    {
        echo('
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-caption-wrap">
                            <a class="navbar-caption text-white display-4" href="login.php">Red social</a>
                        </span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="login.php">
                            <span class="mbri-login mbr-iconfont mbr-iconfont-btn"></span>
                            Log in
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="register.php">
                                <span class="mbri-edit2 mbr-iconfont mbr-iconfont-btn"></span>
                                Register
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        ');
    }
}